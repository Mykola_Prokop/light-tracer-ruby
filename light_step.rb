require 'thrift'
require './thrift/types'
require './client/client_tracer'

module LightStep

	def self.included(klass)
		puts "Module is included in #{klass}"
	end
	###########################################################
		# Initializes and returns the singleton instance of the Tracer.
		# For convenience, multiple calls to initialize are allowed. For example,
		# in library code with more than possible first entry-point, this may
		# be helpful.

		# @return LightStepBase_Tracer
		# @throws Exception if the component name or access token is not a valid string
		# @throws Exception if the tracer singleton has already been initialized

	###########################################################
	def self.initGlobalTracer(component_name, access_token, opts = nil)
		if(component_name.class.name != "String" || component_name.size == 0) 
        puts "Invalid component_name: #{component_name}"
        exit(1)
    end

    if(access_token.class.name != "String" || access_token.size == 0)
    	puts "Invalid access_token"
    	exit(1)
    end

    if(self.nil?)
    	self = self.newTracer(component_name, access_token, opts)
    else
    	if(opts.nil?)
	    	opts = {}
	    end
      self.opts[:component_name] = component_name
      self.opts[:access_token] = access_token
      # self.opts = opts
    end
    self
	end

	###########################################################
		# Returns the singleton instance of the Tracer.
		# 
		# For convenience, this function can be passed the $component_name and
		# $access_token parameters to also initialize the tracer singleton. These
		# values will be ignored on any calls after the first to getInstance().
		# 
		# @param $component_name Component name to use for the tracer
		# @param $access_token The project access token
		# @return LightStepBase_Tracer
		# @throws Exception if the group name or access token is not a valid string
  ###########################################################

  def self.getInstance(component_name = nil, access_token = nil, opts = nil) 
    if(self.nil?)
    	self = self.newTracer(component_name, access_token, opts)
    end
    self
 	end

 	###########################################################
	 	# Creates a new tracer instance.
	 
	  # @param $component_name Component name to use for the tracer
	  # @param $access_token The project access token
	  # @return LightStepBase_Tracer
	  # @throws Exception if the group name or access token is not a valid string.
  ############################################################
 
  def self.newTracer(component_name, access_token, opts = nil)
  	if (opts.nil?) 
      opts = {}
    end
    unless (component_name.nil?) 
      opts[:component_name] = component_name
    end
    unless (access_token.nil?) 
      opts[:access_token] = access_token
    end
    ClientTracer.new(opts)
  end

  ############################################################
  	# Tracer API
	############################################################

	# ==============================================
		# @return LightStepBase_Span
	# ==============================================
	def self.startSpan(operation_name, fields = nil)
		self.getInstance().startSpan(operation_name, fields)
	end

	def self.infof(fmt, *args) 
		self.getInstance.log('I', fmt, args)
  end

  def self.warnf(fmt, *args) 
		self.getInstance.log('W', fmt, args)
  end

  def self.errorf(fmt, *args) 
		self.getInstance.log('E', fmt, args)
  end

  def self.fatalf(fmt, *args) 
		self.getInstance.log('F', fmt, args)
  end

  def self.flush
		self.getInstance.flush
  end

  def self.disable
		self.getInstance.disable
  end

  class << self
    attr_accessor :opts
  end
end